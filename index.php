<!DOCTYPE html>
<html>
<head>
	<title>Steam Web Browser Homepage Generator</title>
</head>
<body>

	<h2>Steam Web Browser Homepage Generator</h2>
	<p>Enter your Steam ID into the field below to generate your personalised homepage.<p>

	<form action="user.php" method="GET">
	  Steam ID: <input type="text" name="id"><br><br>
	  <input type="submit" value="Submit">
	</form>

	<p><b>Examples of acceptable inputs:</b><br>
	- "http://steamcommunity.com/id/scainburger"<br>
	- "scainburger"<br>
	- "http://steamcommunity.com/profiles/76561198008234804"<br>
	- "76561198008234804"</p>

</body>
</html>